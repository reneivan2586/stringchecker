package com.availity.checker;

import static org.junit.jupiter.api.Assertions.*;



import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;



/**
 * @author Rene
 *
 */
class CheckerTest {
	
	static String validText;
	static String validTextV1;
	static String validTextV2;
	static String validTextV3;
	static String stack;
	static char chOpen;
	static char chClose;
	static String tempStack;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		validText = "()[]<>{}";
		validTextV1 = "(<\"";
		validTextV2 = "]}";
		validTextV3 = "()<";
		stack = "";
		chOpen = '(';
		chClose = ')';
		
		
	}
	
	/**
	 * @throws java.lang.Exception
	 */
	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeEach
	void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterEach
	void tearDown() throws Exception {
	}
	



	/**
	 * Test method for {@link com.availity.assesment.Utility#AllParenthesesAreProperlyClosedAndNested(java.lang.String, java.lang.String)}.
	 */
	@Test
	void testAllParenthesesAreProperlyClosedAndNested() {
		assertTrue(Utility.AllParenthesesAreProperlyClosedAndNested(validText, stack)) ;
	}
	
	@Test
	void testAllParenthesesAreProperlyClosedAndNestedV1() {
		assertFalse(Utility.AllParenthesesAreProperlyClosedAndNested(validTextV1, stack)) ;
	}
	
	@Test
	void testAllParenthesesAreProperlyClosedAndNestedV2() {
		assertFalse(Utility.AllParenthesesAreProperlyClosedAndNested(validTextV2, stack)) ;
	}
	
	@Test
	void testAllParenthesesAreProperlyClosedAndNestedV3() {
		assertFalse(Utility.AllParenthesesAreProperlyClosedAndNested(validTextV3, stack)) ;
	}

	/**
	 * Test method for {@link com.availity.assesment.Utility#isOpen(char)}.
	 */
	@Test
	void testIsOpen() {
		assertTrue(Utility.isOpen(chOpen));  
	}

	/**
	 * Test method for {@link com.availity.assesment.Utility#isClose(char)}.
	 */
	@Test
	void testIsClose() {
		assertTrue(Utility.isClose(chClose)); 
	}

	/**
	 * Test method for {@link com.availity.assesment.Utility#isMatching(char, char)}.
	 */
	@Test
	void testIsMatching() {
		assertTrue(Utility.isMatching(chOpen, chClose));
	}

}



