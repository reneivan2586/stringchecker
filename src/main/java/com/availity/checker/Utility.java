package com.availity.checker;

/**
 * 
 * @author Rene
 *
 */

public class Utility {

	private final static String opening = "([{<";
	private final static String closing = ")]}>";

	/*****************************************************************************************************
	 *********** You are tasked to write a checker that validates the parentheses of a LISP **************
	 *********** code.  Write a program (in Java or JavaScript) which takes in a string     **************
	 *********** as an input and returns true if all the parentheses in the string are      **************           
	 *********** properly closed and nested.                                                **************
	 ******************************************************************************************************/
	/*
	 * @param Text to validate
	 * 
	 * @stack string to use as a stack
	 * 
	 * @return True if the text is parentheses is properly closed in other case this
	 * will return false
	 */
	static boolean AllParenthesesAreProperlyClosedAndNested(String text, String stack) {

		return text.isEmpty() ? stack.isEmpty()
				: isOpen(text.charAt(0))
						? AllParenthesesAreProperlyClosedAndNested(text.substring(1), text.charAt(0) + stack)
						: isClose(text.charAt(0))
								? !stack.isEmpty() && isMatching(stack.charAt(0), text.charAt(0))
										&& AllParenthesesAreProperlyClosedAndNested(text.substring(1),
												stack.substring(1))
								: AllParenthesesAreProperlyClosedAndNested(text.substring(1), stack);

	}

	/*
	 * This method will return true if the parameter ch match one of these
	 * characters ([{<
	 * 
	 * @param ch
	 * 
	 * @return true or false
	 */
	static boolean isOpen(char ch) {
		return opening.indexOf(ch) != -1;
	}

	/*
	 * This method will return true if the parameter ch match one of these
	 * characters )]}>
	 * 
	 * @param ch
	 * 
	 * @return true or false
	 */
	static boolean isClose(char ch) {
		return closing.indexOf(ch) != -1;
	}

	/*
	 * This method will return true if the parameter charactersOpen and
	 * CharactersClosed match
	 * 
	 * @param charactersOpen
	 * 
	 * @param charactersClose
	 * 
	 * @return true or false
	 */
	static boolean isMatching(char charactersOpen, char charactersClose) {
		return opening.indexOf(charactersOpen) == closing.indexOf(charactersClose);
	}

}
