package com.availity.checker;

import java.util.Scanner;

/**
 * 
 * @author Rene
 *
 */

public class StringChecker {

	public static void main(String[] args) {

		// Read the string enter
		String stack = "";
		Scanner in = new Scanner(System.in);
		System.out.println("Please enter a text you which to validate");
		String input = in.nextLine();

		try {
			System.out.println(Utility.AllParenthesesAreProperlyClosedAndNested(input, stack));
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			in.close();
		}

	}

}
