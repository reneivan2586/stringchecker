4.	Coding exercise: You are tasked to write a checker that validates the parentheses of a LISP code.  
Write a program (in Java or JavaScript) which takes in a string as an input and returns true if all 
the parentheses in the string are properly closed and nested.


How to run:
           
   1- Open a command prompt with admin priviledge.
   2- run this command java -jar [projectPath]/stringchecker/distribute/stringchecker-Release-1.0.jar
   3- Introduce the string needs to be verified  . Example:  ()[]<>{} 
   5- A message in the console will show the result of the process.(True means it is valid and false means it's not)
   